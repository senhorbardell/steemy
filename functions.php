<?php

update_option('siteurl', 'http://192.168.0.100/senhorbardell');
update_option('home', 'http://192.168.0.100/senhorbardell');

// External link to logo
function wpc_url_login() {
   return "http://senhorbardell.com";
}

add_filter('login_headurl', 'wpc_url_login');

/* login.css 
 * function login_css() {
 *    wp_enqueue_style('login_css', get_template_directory_uri().'/css/login.css');
 * }
 * add_action('login_head', 'login_css');
 */


/* custom wordpress footer
 * function remove_footer_admin() {
 *    echo '&copy; 2012 - wordpress test';
 * }
 * add_filter('admin_footer_text', 'remove_footer_admin');
 */

add_action('init', 'load_scripts');
add_theme_support('post-thumbnails');
set_post_thumbnail_size( 150, 150, true);

function load_scripts() {
	if (!is_admin()) {
		//Load all through yepnope
		wp_enqueue_script( 'modernizr', get_bloginfo('template_directory').'/js/libs/modernizr.js' );
	}
}
if (function_exists('register_nav_menus')) {
   register_nav_menus( array(
      'main' => 'Primary menu',
      'social' => 'Menu for social buttons',
   ) );   
}

/**
 * Customized wordpress excerpt
 * 
 * @param string $text
 */
function improved_trim_excerpt($text) { // Fakes an excerpt if needed
	global $post;
	if ( '' == $text ) {
		$text = get_the_content('');
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
		$text = strip_tags($text);
		$excerpt_length = 55;
		$words = explode(' ', $text, $excerpt_length + 1);
		if (count($words)> $excerpt_length) {
			array_pop($words);
			array_push($words, '<a href="'.get_permalink().'">&#8230;</a>');
			$text = implode(' ', $words);
		}
	}
	return $text;
}

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'improved_trim_excerpt');

/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own twentyten_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' : ?>
		<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
			<div class="vcard">
				<?php echo get_avatar( $comment, 70); ?>
				<div class="author">
					<?php echo get_comment_author_link(); ?>
					<cite><a href="<?php echo esc_url( get_comment_link($comment->comment_ID))?>"><time class="date" pubdate="<?php echo get_comment_date('c'); ?>"><?php printf( '%1$s at %2$s</date>', get_comment_date(), get_comment_time())?></time></a></cite>
				</div>
			</div>
			<div class="text">
				<?php if ($comment->comment_approved == '0'): ?>
					<p><em>Your comment is awaiting moderation.</em></p>
				<?php else: ?>
					<?php comment_text(); ?>
				<?php endif; ?>
			</div>
			<div class="reply">
				<?php edit_comment_link( 'Edit'); ?>
				<?php comment_reply_link( array_merge( $args, array( 'depth'=>$depth, 'max_depth'=>$args['max_depth'] ) ) ); ?>
			</div>
		</li>
		<?php break;
			
		case 'pingback'  :
			
		case 'trackback' :
			break;
			
	endswitch;
}
function twentyten_comment_old( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
			<div class="avatar">
				<?php echo get_avatar( $comment, 70 ); ?>
			</div>
			<div class="comment-body">
				<div class="comment-author vcard">
				
					<?php printf( '%s', sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
					
					<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
					<?php
						/* translators: 1: date, 2: time */
						printf( '%1$s at %2$s', get_comment_date(),  get_comment_time() ); ?></a>
					</div><!-- .comment-meta .commentmetadata -->
					
				</div><!-- .comment-author .vcard -->
				
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em>Your comment is awaiting moderation.</em>
				<?php endif; ?>
		
				<div class="comment-text"><?php comment_text(); ?></div>
		
				<div class="reply">
					<?php edit_comment_link( 'Edit' ); ?>
					<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
				</div><!-- .reply -->
				
			</div><!-- .comment-body -->
		</div><!-- #comment-##  -->
	</li>

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'twentyten' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'twentyten'), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}

/**
 * returns share link
 * @param  string $title blogpost title
 * @param  string $link  blogpost url
 * @return string        encoded link
 */
function twitter_link($title, $link) {
	$share_address = 'https://twitter.com/intent/tweet?';
	$share_address .= 'original_referer='.urlencode($link);
	$share_address .= '&source=tweetbutton';
	$share_address .= '&text='.urlencode($title);
	$share_address .= '&url='.urlencode($link);
	$share_address .= '&via=senhorbardell';
	return $share_address;
} 

add_shortcode('gallery', 'custom_gallery_shortcode');

/**
 * The Gallery shortcode.
 *
 * This implements the functionality of the Gallery Shortcode for displaying
 * WordPress images on a post.
 *
 * @since 2.5.0
 *
 * @param array $attr Attributes attributed to the shortcode.
 * @return string HTML content to display gallery.
 */
function custom_gallery_shortcode($attr) {
	global $post, $wp_locale;

	static $instance = 0;
	$instance++;

	// Allow plugins/themes to override the default gallery template.
	$output = apply_filters('post_gallery', '', $attr);
	if ( $output != '' )
		return $output;

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}

	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post->ID,
		'itemtag'    => 'dl',
		'icontag'    => 'dt',
		'captiontag' => 'dd',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => ''
	), $attr));

	$id = intval($id);
	if ( 'RAND' == $order )
		$orderby = 'none';

	if ( !empty($include) ) {
		$include = preg_replace( '/[^0-9,]+/', '', $include );
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( !empty($exclude) ) {
		$exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if ( empty($attachments) )
		return '';

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment )
			$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
		return $output;
	}

	$itemtag = tag_escape($itemtag);
	$captiontag = tag_escape($captiontag);
	$columns = intval($columns);
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = $gallery_div = '';
	/*if ( apply_filters( 'use_default_gallery_style', true ) )
		$gallery_style = "
		<style type='text/css'>
			#{$selector} {
				margin: auto;
			}
			#{$selector} .gallery-item {
				float: {$float};
				margin-top: 10px;
				text-align: center;
				width: {$itemwidth}%;
			}
			#{$selector} img {
				border: 2px solid #cfcfcf;
			}
			#{$selector} .gallery-caption {
				margin-left: 0;
			}
		</style>
		<!-- see gallery_shortcode() in wp-includes/media.php -->";*/
	$size_class = sanitize_html_class( $size );
	//$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
	$output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );

	$i = 0;
	foreach ( $attachments as $id => $attachment ) {
		$link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);

		//$output .= "<{$itemtag} class='gallery-item'>";
		$output .= "
			<{$icontag} class='gallery-item'>
				$link";
				
			if ( $captiontag && trim($attachment->post_excerpt) )
			$output .= "
				<{$captiontag} class='wp-caption-text gallery-caption'>
				" . wptexturize($attachment->post_excerpt) . "
				</{$captiontag}>";
			
		$output .= "</{$icontag}>";
		//$output .= "</{$itemtag}>";
		if ( $columns > 0 && ++$i % $columns == 0 )
			$output .= '<br style="clear: both" />';
	}

	//$output .= "</div>\n";

	return $output;
}
add_shortcode('wp_caption', 'custom_img_caption_shortcode');
add_shortcode('caption', 'custom_img_caption_shortcode');

/**
 * The Caption shortcode.
 *
 * Allows a plugin to replace the content that would otherwise be returned. The
 * filter is 'img_caption_shortcode' and passes an empty string, the attr
 * parameter and the content parameter values.
 *
 * The supported attributes for the shortcode are 'id', 'align', 'width', and
 * 'caption'.
 *
 * @since 2.6.0
 *
 * @param array $attr Attributes attributed to the shortcode.
 * @param string $content Optional. Shortcode content.
 * @return string
 */
function custom_img_caption_shortcode($attr, $content = null) {

	// Allow plugins/themes to override the default caption template.
	$output = apply_filters('img_caption_shortcode', '', $attr, $content);
	if ( $output != '' )
		return $output;

	extract(shortcode_atts(array(
		'id'	=> '',
		'align'	=> 'alignnone',
		'width'	=> '',
		'caption' => ''
	), $attr));

	if ( 1 > (int) $width || empty($caption) )
		return $content;

	if ( $id ) $id = 'id="' . esc_attr($id) . '" ';

	return '<figure ' . $id . 'class="wp-caption ' . esc_attr($align) . '">'
	. do_shortcode( $content ) . '<figcaption>' . $caption . '</figcaption></figure>';
}
/*
 * For fluid images sake
 */
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
//add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );

function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}


/**
 * Customise the TwentyTen Five comments fields with HTML5 form elements
 *
 *	Adds support for 	placeholder
 *						required
 *						type="email"
 *						type="url"
 *
 * @since TwentyTen Five 1.0
 */
function steemy_comments() {

	$req = get_option('require_name_email');

	$fields =  array(
		'author' => '' .
		            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . $aria_req . '" placeholder = "Name"' . ( $req ? ' required' : '' ) . '/>',
		            
		'email'  => ''.
		            '<input id="email" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] )  . $aria_req . '" placeholder="Email"' . ( $req ? ' required' : '' ) . ' />',
		            
		'url'    => ''.
		            '<input id="url" name="url" type="url" value="' . esc_attr( $commenter['comment_author_url'] ) . '" placeholder="website" />'

	);
	return $fields;
}


function steemy_commentfield() {	

	$commentArea = '<textarea id="comment" name="comment" aria-required="true" required ></textarea>';
	
	return $commentArea;

}


add_filter('comment_form_default_fields', 'steemy_comments');
add_filter('comment_form_field_comment', 'steemy_commentfield');


/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override twentyten_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Twenty Ten 1.0
 * @uses register_sidebar
 */
function steemy_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'twentyten' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The secondary widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 3, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'First Footer Widget Area', 'twentyten' ),
		'id' => 'first-footer-widget-area',
		'description' => __( 'The first footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 4, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Footer Widget Area', 'twentyten' ),
		'id' => 'second-footer-widget-area',
		'description' => __( 'The second footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 5, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Third Footer Widget Area', 'twentyten' ),
		'id' => 'third-footer-widget-area',
		'description' => __( 'The third footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

}
/** Register sidebars by running twentyten_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'steemy_widgets_init' );
