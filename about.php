<?php
/*
 * Template Name: About
 */


/*
 * Data processing
 */
if(isset($_POST['submitted'])) {
	$name = trim($_POST['name']);
	$email = trim($_POST['email']);
	if(function_exists('stripslashes')) $comment = stripslashes(trim($_POST['comment'])); 
	else $comment = trim($_POST['comment']);
	
	$emailTo = get_option('admin_email');
	$subject = $name.' want to contact you.';
	$body = 'Name: $name \n Email: $email \n Comment: $comment';
	$headers = 'From: '.$name.' <'.$emailTo.'>'.'\r\n'.'Reply-To: '.$email;
	
	if (wp_mail($emailTo, $subject, $body, $headers)) $done = true;
	else $done=false;
}
get_header(); ?>

<?php the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header>
			<h1 class="entry-title"><?php the_title(); ?></h1>
		</header>

			<?php the_content(); ?>
			
		<h2>Drop me a few lines</h2>
		<form action="<?php echo site_url('about_post.php'); ?>" id="contact-form" method="post">
			<input type="text" name="name" placeholder="Name" required>
			<input type="email" name="email" placeholder="Email" required>
			<input type="url" name="website" placeholder="website">
			<textarea name="comment" required aria-required="true"></textarea>
			<input type="submit" value="Send" name="submit">
			<input type="hidden" name="submitted" value="true">
			<input type="hidden" name="link" value="<?php echo get_permalink(); ?>">
		</form>
		<?php if(isset($_GET['status'])) echo 'Thank you for your message.'; ?>
		
	</article><!-- #post-## -->

</section><!-- #main -->

<?php get_footer(); ?>