<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Steemy
 * @since Steemy 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) the_post(); ?>

<?php rewind_posts(); ?>

	<article id="post-<?php the_ID(); ?>" class="post <?php if (get_post_custom_values('featured')) echo 'featured'; ?>">
		<h1 class="page-title">
		<?php if ( is_day() ) : ?>
		
			<?php printf( 'Daily Archives: <span>%s</span>' , get_the_date() ); ?>
			
		<?php elseif ( is_month() ) : ?>
		
			<?php printf( 'Monthly Archives: <span>%s</span>' , get_the_date('F Y') ); ?>
			
		<?php elseif ( is_year() ) : ?>
		
			<?php printf( 'Yearly Archives: <span>%s</span>' , get_the_date('Y') ); ?>
			
		<?php else : ?>Blog Archives
		
		<?php endif; ?>
		</h1>
		
		<ul>
			<?php get_template_part( 'loop', 'archive' );?>
		</ul>
		
	</article>
	
<?php get_footer(); ?>
