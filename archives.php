<?php
/*
 * Template name: Archives
 */

get_header(); ?>

<?php the_post(); ?>

   <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <header>
         <h1><?php the_title(); ?></h1>
      </header>
			<?php //wp_get_archives('type=yearly'); ?>
			<?php //wp_get_archives('type=monthly'); ?>
			<?php //wp_list_categories(); ?>
			<?php echo ajaxar_get_years();?>
		
	</article>

<?php get_footer(); ?>
