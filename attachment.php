<?php
/**
 * The template for displaying attachments.
 *
 * @package WordPress
 * @subpackage Twenty_Ten_Five
 * @since Twenty Ten Five 1.0
 */

get_header(); ?>

<?php the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header>
			<h1 class="entry-title">
				<a href="<?php echo get_permalink( $post->post_parent ); ?>" title="<?php esc_attr( printf( __( 'Return to %s', 'twentyten' ), get_the_title( $post->post_parent ) ) ); ?>" rel="portfolio">
					<span>&larr;</span>
					<?php echo get_the_title(); ?>
				</a>
			</h1>
		</header>



					<section class="entry-content">
<?php if ( wp_attachment_is_image() ) :
	$attachments = array_values( get_children( array( 'post_parent' => $post->post_parent, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID' ) ) );
	foreach ( $attachments as $k => $attachment ) {
		if ( $attachment->ID == $post->ID )
			break;
	}
	$k++;
	// If there is more than 1 image attachment in a gallery
	if ( count( $attachments ) > 1 ) {
		if ( isset( $attachments[ $k ] ) )
			// get the URL of the next image attachment
			$next_attachment_url = get_attachment_link( $attachments[ $k ]->ID );
		else
			// or get the URL of the first image attachment
			$next_attachment_url = get_attachment_link( $attachments[ 0 ]->ID );
	} else {
		// or, if there's only 1 image attachment, get the URL of the image
		$next_attachment_url = wp_get_attachment_url();
	}
?>
		<figure class="attachment"><a href="<?php echo $next_attachment_url; ?>" title="<?php echo esc_attr( get_the_title() ); ?>" rel="attachment"><?php
			$attachment_size = apply_filters( 'twentyten_attachment_size', 900 );
			echo wp_get_attachment_image( $post->ID, array( $attachment_size, 9999 ) ); // filterable image width with, essentially, no limit for image height.
		?></a>
		</figure>
	
		<nav>
			<div class="prev"><?php previous_image_link( false, '&larr; Prev'  ); ?></div>
			<div class="middle"><a href="#top" title="back to top">&uarr;</a></div>
			<div class="next"><?php next_image_link(false, 'Next &rarr;'); ?></div>
		</nav><!-- #nav-below -->
		
<?php else : ?>
	<a href="<?php echo wp_get_attachment_url(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" rel="attachment"><?php echo basename( get_permalink() ); ?></a>
<?php endif; ?>
<details>
	<summary>
		<?php
		printf( __('<span class="%1$s">Published</span> %2$s', 'twentyten'),
			'meta-prep meta-prep-entry-date',
			sprintf( '<span class="entry-date"><time datetime="%1$s" class="published" >%2$s</time></span>',
				esc_attr( get_the_date('c') ),
				get_the_date()
			)
		);
		if ( wp_attachment_is_image() ) {
			echo '. ';
			$metadata = wp_get_attachment_metadata();
			printf( __( 'Full size is %s pixels', 'twentyten'),
				sprintf( '<a href="%1$s" title="%2$s">%3$s &times; %4$s</a>',
					wp_get_attachment_url(),
					esc_attr( __('Link to full-size image', 'twentyten') ),
					$metadata['width'],
					$metadata['height']
				)
			);
		}
		?>
		<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="meta-sep">|</span> <span class="edit-link">', '</span>' ); ?>
</summary>		

<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?>
<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>

					<footer class="entry-utility">
						<?php edit_post_link( 'Edit post', ' <span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-utility -->

</details>
					</section><!-- .entry-content -->

				</article><!-- #post-## -->

<?php comments_template(); ?>

<?php get_footer(); ?>
