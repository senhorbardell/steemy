<?php

/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Steemy 
 * @since Steemy 0.1
 */
?><!DOCTYPE html>
<!--[if IE 9 ]>    <html <?php language_attributes(); ?> class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
	
	
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="/favicon.ico">
<link rel="author" href="humans.txt"> 
<?php //<link rel="image_src" href="http://yourdomain.com/logo.png" /> for faceboock thumbnail ?>

<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	wp_head();
?>
</head>

<body <?php body_class(); ?>>

<div id="container" class="clearfix loading" style="opacity:0;">
	<header class="header">
	
		<hgroup>
         <h1 id="site"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
         <div id="sun"></div>
			<em id="site-description"><?php bloginfo( 'description' ); ?></em>
			<nav>
	            <ul id="social">
	               <li><a id="rss" href="http://feeds.feedburner.com/senhorbardell/fjpX">Rss</a></li>
	               <li><a id="twitter" href="http://twitter.com/senhorbardell">Twitter</a></li>
	               <li><a id="facebook" href="http://www.facebook.com/pages/SenhorBardell/72866499468">Facebook</a></li>
	            </ul>
	            <ul id="main-nav">
                  <li id="blog"><a href="<?php echo home_url( '/' ); ?>">Blog</a></li>
	               <li id="about"><a href="./?page_id=50">About</a></li>
	               <li id="portfolio"><a href="./?page_id=38">Portfolio</a></li>
	               <li id="archives"><a href="./?page_id=59">Archives</a></li>
	               <li id="search"> <?php get_search_form(); ?></li>
	            </ul>
	           
	         </nav>
		</hgroup>
			
	</header>

	<section id="main" role="main">
