<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Steemy
 * @since Steemy 1.0
 */

get_header(); ?>

<?php the_post(); ?>

	<a id="top"></a>
	<article id="post-<?php the_ID(); ?>" class="post <?php if (get_post_custom_values('featured')) echo 'featured'; ?>">
		<header>
			<h1 class="entry-title">
			
				<time class="date" pubdate="<?php echo get_the_date('c');?>" title="<?php the_time('l, F jS, Y'); ?> at <?php the_time('g:i a'); ?>">
		         <span class="day"><?php the_date('j'); ?></span>
		       	</time>
		
				<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				<sup class="comments"><?php comments_popup_link('0','1','%'); ?></sup>
				
			</h1>
		</header>

		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>

	<footer class="entry-utility">
		<?php if ( count( get_the_category() ) ) : ?>
			<span class="cat-links">
				<?php echo get_the_category_list( ', ' ); ?>
			</span>
		<?php endif; ?>
		<?php
			$tags_list = get_the_tag_list( '', ', ' );
			if ( $tags_list ):
		?>
			<span class="meta-sep">&rarr;</span>
			<span class="tag-links">
				<?php echo $tags_list ?>
			</span>
		<?php endif; ?>

		<?php edit_post_link( 'Edit', '<span class="meta-sep">|</span> <span class="edit-link">', '</span>' ); ?>
		
		<?php if(function_exists("wpfblike")) echo '<span class="facebook-button">'.wpfblike().'</span>'; ?>
		
		<span class="twitter-button">
			<a href="<?php echo twitter_link(get_the_title(), get_permalink()); ?>" target="_blank">Tweet</a>
         </span>
		
	</footer><!-- .entry-utility -->
</article><!-- #post-## -->

<section id="further-reading" class="navigation clearfix">

	<nav>
		<ul>
			<li class="prev"><?php previous_post_link( '%link', '<span class="meta-nav">&larr;</span> %title' ); ?></li>
			<li class="middle"><a href="#top" title="back to top">&uarr;</a></li>
			<li class="next"><?php next_post_link( '%link', '%title <span class="meta-nav">&rarr;</span>' ); ?></li>
		</ul>
	</nav>

	<div class="featured-posts">
	
		<?php query_posts('meta_key=featured&meta_value=true'); ?>
		
		<?php if (have_posts()): ?>
		<h2>Featured posts</h2>
		<ul>
		<?php while (have_posts()) : the_post(); ?>
			
			<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></li>
			
		<?php endwhile; endif; wp_reset_query(); ?>
		</ul>
		
	</div>
</section>

<?php comments_template( '', true ); ?>

</section><!-- #main -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>

