<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Steemy
 * @since Steemy 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>

	<?php get_template_part( 'loop', 'search' ); ?>
	
<?php else : ?>
	<article id="post-0" class="post no-results not-found">
		<h2 class="entry-title"><?php _e( 'Nothing Found', 'twentyten' ); ?></h2>
		<div class="entry-content">
			<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</article><!-- #post-0 -->
<?php endif; ?>

</section><!-- #main -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
