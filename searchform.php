<?php
/**
 * The template for displaying search forms in Twenty Eleven
 *
 * @package Wordpress
 * @subpackage Steemy
 * @since Steemy 0.1
 */
?>
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input type="search" class="field" name="s" id="s" placeholder="Search. Press '/'" />
	</form>
