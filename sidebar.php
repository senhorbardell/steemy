<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Steemy
 * @since Steemy 1.0
 */
?>

<aside>
	<ul class="xoxo">

	<?php dynamic_sidebar( 'primary-widget-area' ) ?>
		
	</ul>

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
			</ul>
		</div>
	<?php endif; ?>
</aside>

