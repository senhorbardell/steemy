<?php
/**
 * @package WordPress
 * @subpackage Steemy
 * @since Steemy 0.1
 */
?>

<?php get_header(); ?>

	<article id="post-0" class="post error404 not-found">
		<div class="entry-content">
		<figure>
			<img src="<?php echo get_bloginfo('stylesheet_directory').'/img/404.png'; ?>"></img>
		</figure>
			<h1>There is no elephants here!</h1>
		</div><!-- .entry-content -->
	</article><!-- #post-0 -->
</section><!-- #main -->

<?php get_footer(); ?>
