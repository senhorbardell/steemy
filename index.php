<?php
/**
 * @package WordPress
 * @subpackage Steemy
 * @since Steemy 0.1
 */
?>

<?php get_header(); ?>
      
<?php get_template_part( 'loop', 'index' ); ?>

</section><!-- #main -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
