jQuery(document).ready(function ($) {
    $("#searchform").keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).submit();
        }
    });
    $("body").keyup(function (e) {
        if (e.keyCode == 191 && !$('#author').is(':focus') && !$('#email').is(':focus') && !$('#url').is(':focus') && !$('#comment').is(':focus')) {
           $('#s').focus().attr('placeholder', ' ');
        }
    });
    $('#s').focusout(function() {
       $('#s').val() === '' ? $('#s').attr('placeholder', 'Search. Press \''): false;
    });
    $("a[href*=#]").bind("click", function (e) {
        e.preventDefault();
        var link = $(this).attr("href");
        $("html, body").stop().animate({
            scrollTop: 0
        }, 'slow', function () {
            location.hash = link;
        });
        return false;
    });
    $(window).scroll(function () {
        var fringe = false;
        if ($(window).scrollTop() > 103 && !fringe) {
            $("#sun").css("top", "-50px");
            fringe = true;
        } else {
            $("#sun").css("top", "-400px");
            fringe = false;
        }
    });
    $('.twitter-button a').click(function(e) {
        var newWindow = window.open($(this).prop('href'), '', 'height=520,width=550');
        if (window.focus) newWindow.focus();
        return false;
    });
});